# Demo Store project

Proyecto Spring Boot para proveer el precio a aplicar dadas la fecha de aplicación, identificador de producto e identificador de cadena.

## Empezando

Instrucciones de como podemos instalar y configurar el proyecto en nuestro entorno.

### Requisitos previos

Es necesario disponer de Maven y OpenJDK 11 para la ejecución y compilación del proyecto.

### Instalación

Pasos que le indican cómo ejecutar el proyecto en un entorno de desarrollo

```bash
1 - clonar el repositorio: 
git clone https://gitlab.com/fjdelgado/demo-store.git
```
## Uso

Una vez clonado el repositorio, podemos compilar y ejecutarlo haciendo uso de los comandos habituales de maven:
```bash
mvn clean install
mvn test
mvn spring-boot:run
```
### ¿Cómo funciona?

Una vez arrancada la aplicación, esta expone un endpoint para consultar la tarifa a aplicar dados unos parámetros de entrada.
A continuación, podemos ver un ejemplo sobre como realizar una consulta:

**searchProduct** : `POST /demo/api/v1/price/search`
```bash
curl -X 'POST' \
  'http://localhost:8080/demo/api/v1/price/search' \
  -H 'accept: */*' \
  -H 'Content-Type: application/json' \
  -d '{
  "date": "2020-06-14-00.00.00",
  "productId": 35455,
  "brandId": 1
}'
```
*Response:*
```json
 {
  "brandId": 1,
  "productId": 35455,
  "priceList": 1,
  "price": 35.5,
  "date": "2020-06-14-00.00.00"
}
 ```
Además de esto, podemos acceder tanto a la consola de la bbdd h2 como a la interfaz de Swagger. A continuación podemos ver las diferenter urls:

**Swagger:** http://localhost:8080/demo/api/v1/swagger-ui.html

**h2:** http://localhost:8080/demo/api/v1/h2-console

## Autor

Personas que han desarrollado o están al cargo del proyecto.

* Francisco Javier Delgado Vallano - [Fran](mailto:fjdelgado@profile.es)
