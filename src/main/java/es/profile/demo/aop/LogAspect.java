package es.profile.demo.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Aspect
@Component
public class LogAspect {

    private static final String PREFIX = "[ASPECT]";

    @Pointcut("execution(* es.profile.demo.controller..*.*(..))")
    public void inControllerLayer(){}

    @Pointcut("execution(* es.profile.demo.service..*.*(..))")
    public void inServiceLayer(){}

    @Pointcut("execution(* es.profile.demo.converter..*.*(..))")
    public void inConverterLayer(){}

    @Pointcut("execution(* es.profile.demo.repository..*.*(..))")
    public void inRepositoryLayer(){}

    @Around("inControllerLayer() || inServiceLayer() || inConverterLayer() || inRepositoryLayer()")
    public Object logActions(ProceedingJoinPoint joinPoint) throws Throwable {
        return executeLog(joinPoint);
    }

    private Object executeLog(ProceedingJoinPoint pjp) throws Throwable {
        final long init = System.currentTimeMillis();
        final Object output = pjp.proceed();
        final long totalTime = (System.currentTimeMillis() - init);

        final Signature signature = pjp.getSignature();
        final Class<?> returnClass = ((MethodSignature) signature).getReturnType();

        final Logger log = LoggerFactory.getLogger(signature.getDeclaringType().getCanonicalName());

       log.info("{} ({} ms) #{}.{}( {} ) -> {}", PREFIX,
            totalTime, signature.getDeclaringType().getSimpleName(),
            signature.getName(), getParams(pjp.getArgs()),
            getOutput(returnClass.getName(), output));

        return output;
    }

    private String getParams(Object[] args) {
        return Arrays.stream(args).filter(Objects::nonNull).map(Object::toString)
                .collect(Collectors.joining(","));
    }

    private String getOutput(String clazzName, Object output) {
        if ("void".equals(clazzName)) {
            return clazzName;
        }
        return Optional.ofNullable(output).map(Object::toString).map(x -> x.getClass().getName() + ": " + x).orElse("(null)");
    }
}
