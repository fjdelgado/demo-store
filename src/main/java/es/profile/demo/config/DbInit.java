package es.profile.demo.config;

import es.profile.demo.constants.GlobalConstants;
import es.profile.demo.model.Price;
import es.profile.demo.repository.PriceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
@RequiredArgsConstructor
public class DbInit {

    private final PriceRepository priceRepository;

    @PostConstruct
    private void postConstruct(){

        Price p1 = Price.builder()
                .price(35.50)
                .brandId(1)
                .priceList(1)
                .productId(35455)
                .priority(0)
                .startDate(LocalDateTime.parse("2020-06-14-00.00.00", DateTimeFormatter.ofPattern(GlobalConstants.FORMAT_DATETIME)))
                .endDate(LocalDateTime.parse("2020-12-31-23.59.59", DateTimeFormatter.ofPattern(GlobalConstants.FORMAT_DATETIME)))
                .curr("EUR")
                .build();

        Price p2 = Price.builder()
                .price(25.45)
                .brandId(1)
                .priceList(2)
                .productId(35455)
                .priority(1)
                .startDate(LocalDateTime.parse("2020-06-14-15.00.00", DateTimeFormatter.ofPattern(GlobalConstants.FORMAT_DATETIME)))
                .endDate(LocalDateTime.parse("2020-06-14-18.30.00", DateTimeFormatter.ofPattern(GlobalConstants.FORMAT_DATETIME)))
                .curr("EUR")
                .build();

        Price p3 = Price.builder()
                .price(30.50)
                .brandId(1)
                .priceList(3)
                .productId(35455)
                .priority(1)
                .startDate(LocalDateTime.parse("2020-06-15-00.00.00", DateTimeFormatter.ofPattern(GlobalConstants.FORMAT_DATETIME)))
                .endDate(LocalDateTime.parse("2020-06-15-11.00.00", DateTimeFormatter.ofPattern(GlobalConstants.FORMAT_DATETIME)))
                .curr("EUR")
                .build();

        Price p4 = Price.builder()
                .price(38.95)
                .brandId(1)
                .priceList(4)
                .productId(35455)
                .priority(1)
                .startDate(LocalDateTime.parse("2020-06-15-16.00.00", DateTimeFormatter.ofPattern(GlobalConstants.FORMAT_DATETIME)))
                .endDate(LocalDateTime.parse("2020-12-31-23.59.59", DateTimeFormatter.ofPattern(GlobalConstants.FORMAT_DATETIME)))
                .curr("EUR")
                .build();

        priceRepository.saveAll(List.of(p1, p2, p3, p4));
    }
}
