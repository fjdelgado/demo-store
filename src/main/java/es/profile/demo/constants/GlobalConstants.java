package es.profile.demo.constants;

public final class GlobalConstants {
    public static final String FORMAT_DATETIME = "yyyy-MM-dd-HH.mm.ss";
}
