package es.profile.demo.controller;

import es.profile.demo.dto.PriceSearchRequestDTO;
import es.profile.demo.dto.PriceSearchResponseDTO;
import es.profile.demo.service.PriceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/price")
@RequiredArgsConstructor
public class PriceController {

    private final PriceService priceService;

    @PostMapping("/search")
    public ResponseEntity<PriceSearchResponseDTO> searchPrice(@RequestBody final PriceSearchRequestDTO priceSearchRequestDTO){
        return ResponseEntity.ok(priceService.getPriceByDateAndProductAndBrand(priceSearchRequestDTO).get());
    }

}
