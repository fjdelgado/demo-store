package es.profile.demo.converter;

import es.profile.demo.constants.GlobalConstants;
import es.profile.demo.dto.PriceSearchResponseDTO;
import es.profile.demo.model.Price;
import lombok.experimental.UtilityClass;

import java.time.format.DateTimeFormatter;

@UtilityClass
public class PriceConverter {

    public PriceSearchResponseDTO mapToResponseDTO(final Price price){
        return PriceSearchResponseDTO.builder()
                .brandId(price.getBrandId())
                .price(price.getPrice())
                .productId(price.getProductId())
                .priceList(price.getPriceList())
                .date(price.getStartDate().format(DateTimeFormatter.ofPattern(GlobalConstants.FORMAT_DATETIME)))
                .build();
    }

}
