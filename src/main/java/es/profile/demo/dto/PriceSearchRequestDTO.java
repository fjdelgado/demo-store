package es.profile.demo.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
@Builder
public class PriceSearchRequestDTO {
    @NonNull
    private String date;
    private long productId;
    private long brandId;
}
