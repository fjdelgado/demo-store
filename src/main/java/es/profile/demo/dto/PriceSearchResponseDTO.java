package es.profile.demo.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PriceSearchResponseDTO {
    private Long brandId;
    private Long productId;
    private Long priceList;
    private Double price;
    private String date;
}
