package es.profile.demo.repository;

import es.profile.demo.model.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface PriceRepository extends JpaRepository<Price, Long> {
    Optional<List<Price>> findByProductIdAndBrandIdAndEndDateGreaterThanEqualAndStartDateLessThanEqual(long productId, long brandId, LocalDateTime date, LocalDateTime dateTime);
}
