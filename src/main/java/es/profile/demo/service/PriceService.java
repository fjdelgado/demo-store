package es.profile.demo.service;

import es.profile.demo.dto.PriceSearchRequestDTO;
import es.profile.demo.dto.PriceSearchResponseDTO;

import java.util.Optional;

public interface PriceService {
    Optional<PriceSearchResponseDTO> getPriceByDateAndProductAndBrand(PriceSearchRequestDTO priceSearchRequestDTO);
}
