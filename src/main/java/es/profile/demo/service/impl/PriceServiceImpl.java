package es.profile.demo.service.impl;

import es.profile.demo.constants.GlobalConstants;
import es.profile.demo.converter.PriceConverter;
import es.profile.demo.dto.PriceSearchRequestDTO;
import es.profile.demo.dto.PriceSearchResponseDTO;
import es.profile.demo.model.Price;
import es.profile.demo.repository.PriceRepository;
import es.profile.demo.service.PriceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PriceServiceImpl implements PriceService {

    private final PriceRepository priceRepository;

    public Optional<PriceSearchResponseDTO> getPriceByDateAndProductAndBrand(final PriceSearchRequestDTO priceSearchRequestDTO){
        final LocalDateTime requestDate = LocalDateTime.parse(priceSearchRequestDTO.getDate(),
                DateTimeFormatter.ofPattern(GlobalConstants.FORMAT_DATETIME));

        Optional<List<Price>> priceList = priceRepository.findByProductIdAndBrandIdAndEndDateGreaterThanEqualAndStartDateLessThanEqual(
                priceSearchRequestDTO.getProductId(),
                priceSearchRequestDTO.getBrandId(),
                requestDate,
                requestDate
        );

        if(priceList.isPresent() && priceList.get().size() == 1){
            return priceList.get().stream().map(PriceConverter::mapToResponseDTO).findFirst();
        }

        return priceList.map(
                prices -> prices.stream()
                        .filter(p -> p.getPriority() > 0)
                        .map(PriceConverter::mapToResponseDTO)
                        .findFirst()
                        .orElseGet(() -> PriceSearchResponseDTO.builder().build()));

    }
}
