package es.profile.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import es.profile.demo.dto.PriceSearchRequestDTO;
import es.profile.demo.utils.UtilTests;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class PriceControllerTests {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    private static final String CONTEXT_PATH = "/demo/api/v1";
    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("petición a las 10:00 del día 14 del producto 35455 para la brand 1 (ZARA)")
    void searchPrice_whenDateIs14AndHourIs10_ThenOk() throws Exception {
        PriceSearchRequestDTO requestDTO = UtilTests.fillPriceSearchRequest("2020-06-14-10.00.00");

        mockMvc.perform( MockMvcRequestBuilders
                        .post("/price/search")
                        .content(objectMapper.writeValueAsString(requestDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.brandId").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId").value(35455))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price").value(35.5))
                .andExpect(MockMvcResultMatchers.jsonPath("$.priceList").value("1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.date").value("2020-06-14-00.00.00"));
    }

    @Test
    @DisplayName("petición a las 16:00 del día 14 del producto 35455 para la brand 1 (ZARA)")
    void searchPrice_whenDateIs14AndHourIs16_ThenOk() throws Exception {
        PriceSearchRequestDTO requestDTO = UtilTests.fillPriceSearchRequest("2020-06-14-16.00.00");

        mockMvc.perform( MockMvcRequestBuilders
                        .post("/price/search")
                        .content(objectMapper.writeValueAsString(requestDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.brandId").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId").value(35455))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price").value(25.45))
                .andExpect(MockMvcResultMatchers.jsonPath("$.priceList").value("2"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.date").value("2020-06-14-15.00.00"));

    }

    @Test
    @DisplayName("petición a las 21:00 del día 14 del producto 35455 para la brand 1 (ZARA)")
    void searchPrice_whenDateIs14AndHourIs21_ThenOk() throws Exception {
        PriceSearchRequestDTO requestDTO = UtilTests.fillPriceSearchRequest("2020-06-14-21.00.00");

        mockMvc.perform( MockMvcRequestBuilders
                        .post("/price/search")
                        .content(objectMapper.writeValueAsString(requestDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.brandId").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId").value(35455))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price").value(35.5))
                .andExpect(MockMvcResultMatchers.jsonPath("$.priceList").value("1"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.date").value("2020-06-14-00.00.00"));
    }

    @Test
    @DisplayName("petición a las 10:00 del día 15 del producto 35455 para la brand 1 (ZARA)")
    void searchPrice_whenDateIs15AndHourIs10_ThenOk() throws Exception{
        PriceSearchRequestDTO requestDTO = UtilTests.fillPriceSearchRequest("2020-06-15-10.00.00");

        mockMvc.perform( MockMvcRequestBuilders
                        .post("/price/search")
                        .content(objectMapper.writeValueAsString(requestDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.brandId").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId").value(35455))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price").value(30.5))
                .andExpect(MockMvcResultMatchers.jsonPath("$.priceList").value("3"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.date").value("2020-06-15-00.00.00"));
    }

    @Test
    @DisplayName("petición a las 21:00 del día 16 del producto 35455 para la brand 1 (ZARA)")
    void searchPrice_whenDateIs16AndHourIs21_ThenOk() throws Exception{
        PriceSearchRequestDTO requestDTO = UtilTests.fillPriceSearchRequest("2020-06-16-21.00.00");

        mockMvc.perform( MockMvcRequestBuilders
                        .post("/price/search")
                        .content(objectMapper.writeValueAsString(requestDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.brandId").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.productId").value(35455))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price").value(38.95))
                .andExpect(MockMvcResultMatchers.jsonPath("$.priceList").value("4"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.date").value("2020-06-15-16.00.00"));
    }

}
