package es.profile.demo.service.impl;

import es.profile.demo.constants.GlobalConstants;
import es.profile.demo.dto.PriceSearchResponseDTO;
import es.profile.demo.model.Price;
import es.profile.demo.repository.PriceRepository;
import es.profile.demo.utils.UtilTests;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@DisplayName("PriceService unit tests")
public class PriceServiceImplTests {

    @Mock
    private PriceRepository priceRepository;

    @InjectMocks
    private PriceServiceImpl priceService;

    @Test
    void searchPrice_whenDateIs14AndHourIs10_ThenOk(){
        Price price = Price.builder()
                .price(35.50)
                .brandId(1)
                .priceList(1)
                .productId(35455)
                .priority(0)
                .startDate(LocalDateTime.parse("2020-06-14-00.00.00", DateTimeFormatter.ofPattern(GlobalConstants.FORMAT_DATETIME)))
                .endDate(LocalDateTime.parse("2020-12-31-23.59.59", DateTimeFormatter.ofPattern(GlobalConstants.FORMAT_DATETIME)))
                .curr("EUR")
                .build();

        when(priceRepository.findByProductIdAndBrandIdAndEndDateGreaterThanEqualAndStartDateLessThanEqual(anyLong(), anyLong(), any(), any()))
                .thenReturn(Optional.of(Lists.list(price)));

        Optional<PriceSearchResponseDTO> responseDTO =
                priceService.getPriceByDateAndProductAndBrand(UtilTests.fillPriceSearchRequest("2020-06-14-10.00.00"));

        assertNotNull(responseDTO);
        assertTrue(responseDTO.isPresent());
        assertEquals(responseDTO.get().getPrice(),35.5);
        assertEquals(responseDTO.get().getDate(), "2020-06-14-00.00.00");
        assertEquals(responseDTO.get().getBrandId(), 1);
        assertEquals(responseDTO.get().getPriceList(), 1);
    }
}
