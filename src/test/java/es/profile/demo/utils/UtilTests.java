package es.profile.demo.utils;

import es.profile.demo.dto.PriceSearchRequestDTO;
import lombok.experimental.UtilityClass;

@UtilityClass
public class UtilTests {

    public PriceSearchRequestDTO fillPriceSearchRequest(final String date){
        return PriceSearchRequestDTO.builder()
                .brandId(1)
                .productId(35455)
                .date(date)
                .build();
    }

}
